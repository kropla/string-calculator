package dev.kropla.calc;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by kropla on 26.02.17.
 */
public class Calculator {
    private static final String DEFAULT_DELIMETER = ",|\n";
    private static final String SPECIFIC_DELIMETER_START_SYMBOL = "//";
    public static final int MAX_NUMBER_VALUE = 1000;
    private String delimeter;
    private int[] numbersFromArgument;
    private String leftStringFromArg;

    public int add(String numbersArg) {
        leftStringFromArg = numbersArg;

        if (numbersArg.isEmpty()) {
            return 0;
        }
        extractOperandsFromArgumentString();
        checkNegativeNumbers();
        return doSum();
    }

    private void extractOperandsFromArgumentString() {
        extractDelimeter();
        extractOperandsNumbers();
    }

    private void extractDelimeter() {
        if (argumentHasSpecificDelimiter()) {
            delimeter = extractSpecificDelimeter();
            delimeter = formatIfMultipleDelimetersExists();
        } else {
            delimeter = DEFAULT_DELIMETER;
        }
    }

    private boolean argumentHasSpecificDelimiter() {
        return leftStringFromArg.startsWith(SPECIFIC_DELIMETER_START_SYMBOL);
    }

    private String extractSpecificDelimeter() {
        String [] cuts = leftStringFromArg.split("\\n", 2);
        leftStringFromArg = cuts[1];
        return cuts[0].substring(2);
    }

    private String formatIfMultipleDelimetersExists() {
        return Arrays.stream(delimeter.split("\\|"))
                .map(Pattern::quote)
                .collect(Collectors.joining("|"));
    }



    private void extractOperandsNumbers() {
        String numbersInString = leftStringFromArg;

        numbersFromArgument = extractNumbersByCriteria(numbersInString);
    }

    private int[] extractNumbersByCriteria(String numbersInString) {
        return Arrays.stream(numbersInString.split(delimeter))
                .mapToInt(Integer::parseInt)
                .filter(n -> n <= MAX_NUMBER_VALUE)
                .toArray();
    }


    private void checkNegativeNumbers() {
        if (negativeNumberIsInArgument()) {
            throw new IllegalArgumentException("negatives not allowed: " + gatherAllNegativesInStringMsg());
        }
    }

    private boolean negativeNumberIsInArgument() {
        return Arrays.stream(numbersFromArgument).anyMatch(i -> i < 0);
    }

    private String gatherAllNegativesInStringMsg() {
        List<String> negatives = Arrays.stream(numbersFromArgument)
                .filter(i -> i < 0)
                .mapToObj(i -> i + ";")
                .collect(Collectors.toList());

        StringBuilder sb = new StringBuilder();
        negatives.forEach(n -> sb.append(n));
        return sb.toString();
    }

    private int doSum() {
        return Arrays.stream(numbersFromArgument).sum();
    }
}
