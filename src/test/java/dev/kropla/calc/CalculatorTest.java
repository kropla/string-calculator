package dev.kropla.calc;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Created by kropla on 26.02.17.
 */
public class CalculatorTest {
    private Calculator calc;


    @Before
    public void initCalc() {
        calc = new Calculator();

    }

    @Test
    public void shouldReturnZero_whenInputStringIsEmpty() {
        assertEquals("when input is empty should return 0", 0, calc.add(""));
    }

    @Test
    public void shouldReturnArgumentValue_whenInputHasOnlyOneNumber() {
        assertEquals("when input has only one number, return this number", 2, calc.add("2"));
    }

    @Test
    public void shouldReturnSum_whenInputHasTwoNumbersCommaSeparated() {
        assertEquals("when input has two numbers, return theirs sum", 5, calc.add("2,3"));
    }

    @Test
    public void shouldReturnSum_whenInputTenNumbersCommaSeparated() {
        assertEquals("when input has ten numbers, return theirs sum", 45, calc.add("1,2,3,4,5,6,7,8,9,0"));
    }

    @Test
    public void shouldReturnSum_whenUsingNewLineAsDelimiter() {
        assertEquals( 6, calc.add("1\n5"));
    }

    @Test
    public void shouldReturnSum_whenUsingNewLineAndCommaAsDelimiter() {
        assertEquals( 6, calc.add("1\n2,3"));
    }

    @Test
    public void shouldReturnSum_whenUsingSpecifiedDelimiterOnTheArgumentBegining() {
        assertEquals(3, calc.add("//;\n1;2"));
        assertEquals(6, calc.add("//;\n1;2;3"));
        assertEquals(5, calc.add("//.\n2.3"));
    }

    @Rule
    public ExpectedException expExc = ExpectedException.none();


    @Test
    public void shouldThrowException_whenNegativeNumberIsOnArgument() {
        expExc.expect(IllegalArgumentException.class);
        expExc.expectMessage("negatives not allowed: -2;");
        calc.add("1,-2");
    }

    @Test
    public void shouldThrowExceptionWithNumberList_whenMoreNegativesNumberIsOnArgument() {
        expExc.expect(IllegalArgumentException.class);
        expExc.expectMessage("negatives not allowed: -2;-5;-122");
        calc.add("1,-2,9,-5,-122");
    }

    @Test
    public void shouldIgnoreNumbersBiggerThanOneThousend() {
        assertEquals( "number bigger than 1000 shouldn't be calculated", 2, calc.add("2,1001"));
        assertEquals( "1000 should be calculated", 1002, calc.add("2,1000"));
    }


    @Test
    public void delimetersCanBeOfAnyLength() {
        assertEquals( 6, calc.add("//***\n1***2***3"));
        assertEquals( 7, calc.add("//....\n2....2....3"));
    }


    @Test
    public void handleMultipleDelimeters() {
        assertEquals( 6, calc.add("//*|%\n1*2%3"));
    }

    @Test
    public void handleMultipleDelimetersWithLengthMoreThanOneChar() {
        assertEquals( 6, calc.add("//*|%%\n1*2%%3"));
    }

}
