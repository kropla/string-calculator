create method "add" for the simple calculator, which returns sum of all integers that were in String input argument.
int add(String numbers);


1. when attribute numbers string is empty return 0;
2. when numbers has only one number return this number;
3. when there are two numbers return their sum as a result;
4. handle an unknown amount of numbers
5. handle the new line separator between the numbers
6. support different delimeter specified on the begining of the string argument
7. negative numbers should throw exception
8. should print all negatives numbers passed
9. numbers bigger than 1000 should be ignored
10. number equal 1000 should be calculated
11. delimeters of any length
12. accept multiple delimeters
13. accept multiple delimeters with length longer than one char